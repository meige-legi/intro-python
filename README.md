# Introduction to Python for sciences (Master 1)

## The teacher

- Pierre Augier: researcher at [LEGI](www.legi.grenoble-inp.fr) studying
geophysical turbulence with experiments and numerical simulations. Maintainer
of the [FluidDyn project](https://fluiddyn.readthedocs.io).

## Setup the environment for this course

See the file
[install.md](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/intro-python/-/blob/master/install.md).
Note that you can copy/paste commands!

## Clone this repository

Clone the repository with Mercurial (and the extension hg-git, as explained
[here](https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html)):

```
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/intro-python.git
```

## Slides on the web

The slides of this course are also hosted
[here](https://meige-legi.gricad-pages.univ-grenoble-alpes.fr/intro-python/).

## Play with (or display) the notebooks

To modify the notebooks:

```
cd ipynb
jupyter-lab
```

To see the presentations made from the notebooks:

```
make presentations
make serve
```

## More advanced training

If you feel that you need more advanced content, you can work on this [Python
HPC
training](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc)
(the associated slices are
[here](https://python-uga.gricad-pages.univ-grenoble-alpes.fr/training-hpc/)).
